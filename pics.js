var express = require('express');
var cors = require('cors');
var router = express.Router();
var Feed = require('rss-to-json');

router.get('/', function(req, res){
   res.send('Flickr Picture Feed Reader API');
});

router.get('/pics', cors(), function(req, res){ 
   Feed.load('https://www.flickr.com/services/feeds/photos_public.gne', function(err, rss){
      res.json(rss);
   });
});

module.exports = router;