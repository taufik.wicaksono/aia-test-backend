var express = require('express');
var app = express();
var pics = require('./pics');
var bodyParser = require('body-parser');

app.use('/', pics);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}
app.listen(port);
